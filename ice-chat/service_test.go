package iceChat

import (
	"testing"
)

func TestNewStartServiceRequest(t *testing.T) {
	service, version := "Pricing", "Equities 1.7"
	expected := `<?xml version="1.0" encoding="UTF-8"?><Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><Content xsi:type="StartServiceRequest"><Version>Equities 1.7</Version><Service>Pricing</Service></Content></Envelope>`

	result, err := newStartServiceRequest(service, version)
	if err != nil {
		t.Errorf("Expected to create xml message, error: %s", err.Error())
	}

	actual := string(result)

	if actual != expected {
		t.Errorf("Expected/got:\n%s\n%s", expected, actual)
	}
}
