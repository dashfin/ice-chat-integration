package iceChat

import (
	"bytes"
	"encoding/xml"
	"fmt"
)

func newStartServiceRequest(service, version string) ([]byte, error) {

	buff := bytes.NewBufferString(`<?xml version="1.0" encoding="UTF-8"?>`)
	enc := xml.NewEncoder(buff)

	reqXml := EnvelopeStartService{}
	reqXml.Xsi = "http://www.w3.org/2001/XMLSchema-instance"
	reqXml.Content.Type = "StartServiceRequest"
	reqXml.Content.Version = version
	reqXml.Content.Service = service

	err := enc.Encode(reqXml)
	if err != nil {
		return nil, fmt.Errorf("failed to encode StartServiceRequest: %w", err)
	}

	return buff.Bytes(), nil
}

func newStopServiceRequest(service string) ([]byte, error) {

	buff := bytes.NewBufferString(`<?xml version="1.0" encoding="UTF-8"?>`)
	enc := xml.NewEncoder(buff)

	reqXml := EnvelopeStartService{}
	reqXml.Xsi = "http://www.w3.org/2001/XMLSchema-instance"
	reqXml.Content.Type = "StartStopRequest"
	reqXml.Content.Service = service

	err := enc.Encode(reqXml)
	if err != nil {
		return nil, fmt.Errorf("failed to encode StopServiceRequest: %w", err)
	}

	return buff.Bytes(), nil
}
