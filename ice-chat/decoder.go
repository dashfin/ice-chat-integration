package iceChat

import (
	"encoding/xml"
	"golang.org/x/text/encoding/unicode"
	"golang.org/x/text/transform"
	"io"
)

func newDecoder(input io.Reader) *xml.Decoder {
	decoder := xml.NewDecoder(toUTF8(input))
	decoder.CharsetReader = noopCharsetReader
	return decoder
}

func noopCharsetReader(_ string, input io.Reader) (io.Reader, error) {
	return input, nil
}

func toUTF8(r io.Reader) io.Reader {
	return transform.NewReader(r, unicode.UTF16(unicode.LittleEndian, unicode.UseBOM).NewDecoder())
}
