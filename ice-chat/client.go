package iceChat

import (
	"context"
	"errors"
	"fmt"
	"io"
	"log"
	"net"
	"time"
)

type (
	Client struct {
		host, port       string
		reconnectTimeout time.Duration
		logger           *log.Logger
		conn             net.Conn
		msgCh            chan Envelope
		doneCh           chan struct{}
		ctx              context.Context
		cancel           context.CancelFunc
	}
	OptionsFunc func(*Client)
)

func NewClient(host, port string, options ...OptionsFunc) *Client {
	ctx, cancel := context.WithCancel(context.Background())
	c := &Client{
		host:             host,
		port:             port,
		msgCh:            make(chan Envelope),
		doneCh:           make(chan struct{}),
		reconnectTimeout: 5 * time.Second,
		ctx:              ctx,
		cancel:           cancel,
	}

	for _, option := range options {
		option(c)
	}

	return c
}

func (c *Client) Listen() {

	for {

		var err error
		if c.conn, err = c.connect(); err == nil {
			err = c.consume(c.conn)
			if err == nil {
				return
			}
			if err := c.ctx.Err(); err != nil {
				return
			}
		}

		c.log("Failed to process: %s", err.Error())
		time.Sleep(c.reconnectTimeout)
	}
}

func (c Client) Incoming() <-chan Envelope {
	return c.msgCh
}

func (c Client) Close() error {
	c.log("Closing...")
	c.cancel()

	if c.conn == nil {
		return nil
	}

	if err := c.closePricingService(c.conn); err != nil {
		c.log("Failed to send StopServiceRequest: %s", err.Error())
	}

	return c.conn.Close()
}

func (c Client) log(format string, args ...interface{}) {
	if c.logger != nil {
		c.logger.Printf(format, args...)
	}
}

func (c Client) startPricingService(conn net.Conn) error {
	var err error
	var startRequest []byte

	if startRequest, err = newStartServiceRequest("Pricing", "Equities 1.7"); err != nil {
		return err
	}

	if _, err = conn.Write(startRequest); err != nil {
		return err
	}
	return nil
}

func (c Client) closePricingService(conn net.Conn) error {
	var err error
	var closeRequest []byte

	if closeRequest, err = newStopServiceRequest("Pricing"); err != nil {
		return err
	}

	if _, err = conn.Write(closeRequest); err != nil {
		return err
	}
	return nil
}

func (c Client) consume(input io.Reader) error {

	decoder := newDecoder(input)

	for {
		en := Envelope{}
		if err := decoder.Decode(&en); err != nil {
			if err == io.EOF {
				return errors.New("EOF error")
			}

			if err, ok := err.(*net.OpError); ok {
				return fmt.Errorf("network error: %s", err)
			}

			c.log("error decoding message: %s", err.Error())
		} else {
			c.msgCh <- en
		}
	}
}

func (c *Client) connect() (net.Conn, error) {
	var err error
	c.log("Connecting to %s:%s...", c.host, c.port)
	conn, err := net.DialTimeout("tcp", c.host+":"+c.port, c.reconnectTimeout)
	if err != nil {
		return nil, fmt.Errorf("cannot start call on %s:%s, error: %w", c.host, c.port, err)
	}

	c.log("Connected")

	if err = c.startPricingService(conn); err != nil {
		return nil, fmt.Errorf("failed to start pricing service: %w", err)
	}
	c.log("Service started")
	return conn, nil
}

func WithLogger(logger *log.Logger) OptionsFunc {
	return func(client *Client) {
		client.logger = logger
	}
}

func WithReconnectTimeout(d time.Duration) OptionsFunc {
	return func(client *Client) {
		client.reconnectTimeout = d
	}
}
