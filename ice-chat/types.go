package iceChat

import (
	"encoding/xml"
	"fmt"
	"time"
)

type (
	// todo: key-value
	EnvelopeStartService struct {
		XMLName xml.Name `xml:"Envelope"`
		//Text    string   `xml:",chardata"`
		Xsi     string `xml:"xmlns:xsi,attr"`
		Content struct {
			//Text    string `xml:",chardata"`
			Type    string `xml:"xsi:type,attr"`
			Version string `xml:"Version"`
			Service string `xml:"Service"`
		} `xml:"Content"`
	}
	EnvelopStopService struct {
		XMLName xml.Name `xml:"Envelope"`
		Xsi     string `xml:"xmlns:xsi,attr"`
		Content struct {
			Type    string `xml:"xsi:type,attr"`
			Service string `xml:"Service"`
		} `xml:"Content"`
	}
	Envelope struct {
		XMLName xml.Name `xml:"Envelope"`
		//Text      string      `xml:",chardata"`
		Xsi     string      `xml:"xmlns:xsi,attr"`
		Content PriceUpdate `xml:"Content"`
	}
	KeyValue    map[string]string
	Participant struct {
		Name string `xml:"Name"`
		Type string `xml:"Type"`
	}
	Underlying struct {
		Symbol string `xml:"Symbol"`
		Name   string `xml:"Name"`
	}
	Quote struct {
		ID             string      `xml:"Id"`
		Sender         Participant `xml:"Sender"`
		Receiver       Participant `xml:"Receiver"`
		SentOrReceived string      `xml:"SentOrReceived"`
		Bid            float64     `xml:"Bid"`
		Ask            float64     `xml:"Ask"`
		QuoteTime      Time        `xml:"QuoteTime"`
		BidSize        int         `xml:"BidSize"`
		AskSize        int         `xml:"AskSize"`
	}
	OptionLeg struct {
		OptionType     string     `xml:"OptionType"`
		LegId          string     `xml:"LegId"`
		ExerciseType   string     `xml:"ExerciseType"`
		Description    string     `xml:"Description"`
		Term           string     `xml:"Term"`
		ExpirationDate Time       `xml:"ExpirationDate"`
		StrikeType     string     `xml:"StrikeType"`
		Strike         float64    `xml:"Strike"`
		Ratio          float64    `xml:"Ratio"`
		Direction      int        `xml:"Direction"` // todo custom type
		Underlying     Underlying `xml:"Underlying"`
		OSICode        string     `xml:"OSICode"`
		CrossId        string     `xml:"CrossId"`
	}
	MarketDetail struct {
		//Text        string      `xml:",chardata"`
		ID          string      `xml:"Id"`
		Description string      `xml:"Description"`
		Strategy    string      `xml:"Strategy"`
		LastQuote   Quote       `xml:"LastQuote"`
		OptionLegs  []OptionLeg `xml:"OptionLegs"`
	}
	PriceUpdate struct {
		//Text         string       `xml:",chardata"`
		Type         string       `xml:"type,attr"`
		Action       string       `xml:"Action"`
		User         string       `xml:"User"`
		MarketDetail MarketDetail `xml:"MarketDetail"`
	}

	Time struct {
		time.Time
	}
)

func (t *Time) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	formats := [...]string{"2006-01-02T15:04:05.999-07:00", "2006-01-02T15:04:05"}
	var parsed time.Time
	var err error
	var v string
	_ = d.DecodeElement(&v, &start)

	for _, format := range formats {
		parsed, err = time.Parse(format, v)
		if err == nil {
			*t = Time{parsed}
			return nil
		}
	}

	return fmt.Errorf("cannot parse time %s", v)
}
