package main

import "C"
import (
	iceChat "bitbucket.org/dashfin/ice-chat-integration.git/ice-chat"
	"fmt"
	"log"
	"os"
)

const host = "172.20.65.14"
const port = "5511"

func handleMessage(incoming <-chan iceChat.Envelope) {
	for m := range incoming {
		fmt.Printf("DEADBEEF main.go:15 - m %v\n", m)
	}
}

//export Start
func Start() {

	logger := log.New(os.Stdout, "", log.LstdFlags)
	iceChatLogger := log.New(logger.Writer(), "[ICE Chat Client] ", log.LstdFlags)

	client := iceChat.NewClient(host, port, iceChat.WithLogger(iceChatLogger))

	go handleMessage(client.Incoming())

	client.Listen()
}

func main() {}
