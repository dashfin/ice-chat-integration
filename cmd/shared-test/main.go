package main

import "C"
import "fmt"

//export SayHello
func SayHello(name string) string {
	return fmt.Sprintf("Hello %s!", name)
}

//export SayHelloCB
func SayHelloCB(name string, cb func(string)) {
	cb(SayHello(name))
}

func main() {}
