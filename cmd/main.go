package main

import (
	iceChat "bitbucket.org/dashfin/ice-chat-integration.git/ice-chat"
	"fmt"
	"io"
	"log"
	"os"
	"os/signal"
	"time"
)

const host = "172.20.65.14"
const port = "5511"

func listenMessages(incoming <-chan iceChat.Envelope) {
	for m := range incoming {
		fmt.Printf("DEADBEEF main.go:15 - m %v\n", m)
	}
}

func listenToExit(c io.Closer) {
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt)
	<-ch
	if err := c.Close(); err != nil {
		log.Fatalf("Closed with error %s\n", err.Error())
	}
}

func main() {

	logger := log.New(os.Stdout, "", log.LstdFlags)
	iceChatLogger := log.New(logger.Writer(), "[ICE Chat Client] ", log.LstdFlags)

	client := iceChat.NewClient(host, port, iceChat.WithLogger(iceChatLogger), iceChat.WithReconnectTimeout(time.Second))

	go listenToExit(client)

	go listenMessages(client.Incoming())

	client.Listen()
}
