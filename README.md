## Build

### Build-time dependencies

As build target of the project is Windows DLL we can't se Go cross-compile feature out of the box.

There are a few options for building the DLL:
1. Use Windows machine to create a build.
2. Use Linux machine to create a build.
3. Use prepared Docker image for building 

#### Building using Windows-based machine

Prerequisites: 
- Go v1.15
- C/C++ compiler \
(tested with mingw: `gcc version 8.1.0 (x86_64-posix-seh-rev0, Built by MinGW-W64 project)`)

Build command:
```
go build -buildmode=c-shared -o=libicechat.dll ./cmd/shared/main.go 
```
This command will produce `libicechat.dll` and `libicechat.h` files.

#### Building using Linux-based machine

Prerequisites: 
- Go v1.15
- C/C++ cross-compiler

In this case build command requires special environment variables

```
CC=x86_64-w64-mingw32-gcc \ 
CXX=x86_64-w64-mingw32-g++ \
CGO_ENABLED=1 \
GOOS=windows \
GOARCH=amd64 \
go build -v -buildmode=c-shared -o=libicechat.dll ./cmd/shared/main.go 
```

#### Building using Docker image

You may find docker image that contain required tools [here](https://hub.docker.com/r/ridgea/go-crosscompile)

```
docker run -it --rm \
-v $PWD:/go/package/app \
ridgea/go-crosscompile:1.15 \
-buildmode=c-shared -o=libicechat.dll ./cmd/shared/main.go
```
